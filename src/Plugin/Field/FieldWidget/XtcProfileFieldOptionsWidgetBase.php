<?php

namespace Drupal\xtcfield\Plugin\Field\FieldWidget;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

/**
 * Class XtcFieldOptionsWidgetBase
 * @inheritDoc
 *
 * @package Drupal\xtcfield\Plugin\Field\FieldWidget
 */
abstract class XtcProfileFieldOptionsWidgetBase extends XtcFieldOptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Prepare some properties for the child methods to build the actual form
    // element.
    $this->required = $element['#required'];
    $this->multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple();
    $this->has_value = FALSE;

    // Add our custom validator.
    $element['#element_validate'][] = [get_class($this), 'validateElement'];
    $element['#key_column'] = $this->column;

    // The rest of the $element is built by child method implementations.

    return $element;
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */




//  protected function getOptions() {
  protected function getOptions(FieldableEntityInterface $entity) {
    $options = [];
    if (!isset($this->options)) {
      $fieldSettings = $this->getFieldSettings();
      $profile = $fieldSettings['xtcprofile'];
      $xtcoptions = ToolBox::splitPipe($fieldSettings['xtcoptions']);
      $key = $xtcoptions['key'];
      $label = $xtcoptions['label'];

      if (!empty($fieldSettings['count'])) {
        $xtcoptions['size'] = $fieldSettings['count'];
      }
      $values = XtcLoaderProfile::content($profile, $xtcoptions);
      $results = Json::decode($values);
      foreach ($results as $result) {
//        $options[$result[$key]] = $result[$label];
        $options[$result[$key]] = $result[$key] . ' — ' . $result[$label];      }

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      // Options might be nested ("optgroups"). If the widget does not support
      // nested options, flatten the list.
      if (!$this->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }

      $this->options = $options;
    }
    return $this->options;
  }


  /**
   * Determines selected options from the incoming field values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values.
   *
   * @return array
   *   The array of corresponding selected options.
   */
  protected function getSelectedOptions(FieldItemListInterface $items) {
    // We need to check against a flat list of options.
    $flat_options = OptGroup::flattenOptions($this->getOptions($items->getEntity()));

    $selected_options = [];
    foreach ($items as $item) {
      $value = $item->{$this->column};
      // Keep the value if it actually is in the list of options (needs to be
      // checked against the flat list).
      if (isset($flat_options[$value])) {
        $selected_options[] = $value;
      }
    }
    return $selected_options;
  }
}
