<?php

namespace Drupal\xtcfield\Plugin\Field\FieldWidget;


use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteMatch;
use Drupal\xtc\XtendedContent\API\ToolBox;
use Drupal\xtcentity\Entity\XtendedContentType;
use Symfony\Component\Routing\Route;

/**
 * Class XtcFieldOptionsWidgetBase
 * @inheritDoc
 *
 * @package Drupal\xtcfield\Plugin\Field\FieldWidget
 */
abstract class XtcFieldOptionsWidgetBase extends OptionsWidgetBase
{

  /**
   * @var array
   */
  var $xtchandlers = [];

  /**
   * @var
   */
  var $bundle;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    // Prepare some properties for the child methods to build the actual form
    // element.
    $this->required = $element['#required'];
    $this->multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple();
    $this->has_value = FALSE;
    $this->xtchandlers = $form_state->get('xtchandlers');

    // Add our custom validator.
    $element['#element_validate'][] = [get_class($this), 'validateElement'];
    $element['#key_column'] = $this->column;

    // The rest of the $element is built by child method implementations.

    return $element;
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity)
  {
    if (!isset($this->options)) {
      if('xtcfield_plugin_profile' == $this->getType()) {
        $this->options = ToolBox::getPluginsByHandlers($this->getService(), $this->xtchandlers);
      }
      else {
        $this->options = ToolBox::getPlugins($this->getService());
      }
    }
    return $this->options;
  }

  protected function getService()
  {
    $field_types = \Drupal::service('plugin.manager.field.field_type')->getDefinitions();
    return $field_types[$this->getType()]['service'];
  }

  protected function getType()
  {
    return $this->fieldDefinition->getType();
  }

  protected function isFieldable()
  {
    $field_types = \Drupal::service('plugin.manager.field.field_type')->getDefinitions();
    return $field_types[$this->getType()]['fieldable'] ?? false;
  }

  /**
   * Determines selected options from the incoming field values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values.
   *
   * @return array
   *   The array of corresponding selected options.
   */
  protected function getSelectedOptions(FieldItemListInterface $items)
  {
    // We need to check against a flat list of options.
    $flat_options = OptGroup::flattenOptions($this->getOptions($items->getEntity()));

    $selected_options = [];
    foreach ($items as $item) {
      $value = $item->{$this->column};
      // Keep the value if it actually is in the list of options (needs to be
      // checked against the flat list).
      if (isset($flat_options[$value])) {
        $selected_options[] = $value;
      }
    }
    return $selected_options;
  }
}
