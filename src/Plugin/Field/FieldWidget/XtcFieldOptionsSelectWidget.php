<?php

namespace Drupal\xtcfield\Plugin\Field\FieldWidget;


use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'xtcfield_select_widget' widget.
 *
 * @FieldWidget(
 *   id = "xtcfield_options_select",
 *   label = @Translation("XTC Field Select list"),
 *   field_types = {
 *     "xtcfield_plugin_cache",
 *     "xtcfield_plugin_handler",
 *     "xtcfield_plugin_profile",
 *     "xtcfield_plugin_request",
 *     "xtcfield_plugin_server",
 *   },
 *   multiple_values = TRUE
 * )
 */
class XtcFieldOptionsSelectWidget extends XtcFieldOptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $options = [
      '' => '-----',
      ];

    $element += [
      '#type' => 'select',
//      '#options' => $this->getOptions(),
      '#options' => array_merge($options, $this->getOptions($items->getEntity())),
      '#default_value' => $this->getSelectedOptions($items),
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => $this->multiple && count($this->options) > 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if ($this->multiple) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return t('- None -');
      }
    }
    else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return t('- None -');
      }
      if (!$this->has_value) {
        return t('- Select a value -');
      }
    }
  }

}
