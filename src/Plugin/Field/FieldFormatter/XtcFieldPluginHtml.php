<?php

namespace Drupal\xtcfield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

/**
 * Plugin implementation of the 'xtcfield_plugin_html_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "xtcfield_plugin_html_formatter",
 *   label = @Translation("HTML"),
 *   field_types = {
 *     "xtcfield_plugin_cache",
 *     "xtcfield_plugin_handler",
 *     "xtcfield_plugin_profile",
 *     "xtcfield_plugin_request",
 *     "xtcfield_plugin_server",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class XtcFieldPluginHtml extends XtcFieldPluginLabel {

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $name =  $item->getString();
    $content = XtcLoaderProfile::content($name);
    return $content;
  }

}
