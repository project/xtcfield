<?php

namespace Drupal\xtcfield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

/**
 * Plugin implementation of the 'xtcfield_plugin_debug_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "xtcfield_plugin_debug_formatter",
 *   label = @Translation("Debug"),
 *   field_types = {
 *     "xtcfield_plugin_cache",
 *     "xtcfield_plugin_handler",
 *     "xtcfield_plugin_profile",
 *     "xtcfield_plugin_request",
 *     "xtcfield_plugin_server",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class XtcFieldPluginDebug extends XtcFieldPluginLabel {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }
    // Not to cache this field formatter.
    $elements['#cache']['max-age'] = 0;

    return $elements;
  }


  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $name =  $item->getString();
    $content = XtcLoaderProfile::content($name);
    dump($content);
    return null;
  }

}
