<?php

namespace Drupal\xtcfield\Plugin\Field\FieldType;


/**
 * Plugin implementation of the 'xtcfield_plugin_server' field type.
 *
 * @FieldType(
 *   id = "xtcfield_plugin_server",
 *   label = @Translation("XTC Server plugin"),
 *   description = @Translation("Select an XTC Server"),
 *   category = @Translation("XTC Plugin Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_server",
 * )
 */
class XtcFieldPluginServer extends XtcFieldPluginBase {

}
