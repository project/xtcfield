<?php

namespace Drupal\xtcfield\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\xtc\XtendedContent\API\ToolBox;

/**
 * Plugin implementation of the 'xtcfield_profile' field type.
 *
 * @FieldType(
 *   id = "xtcfield_profile",
 *   label = @Translation("XTC Fieldable Profile"),
 *   description = @Translation("Access data through an XTC Profile"),
 *   category = @Translation("XTC Fields"),
 *   default_widget = "xtcfield_options_select",
 *   default_formatter = "xtcfield_label_formatter",
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   service = "plugin.manager.xtc_profile",
 *   fieldable = true,
 * )
 */
class XtcFieldProfile extends XtcFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'max_length' => 255,
        'xtcprofile' => '',      //Default XTC Profile
        'xtcoptions' => '',      //Default Options for XTC Profile
        'xtcprofile_item' => '',      //Default XTC Profile - Item
        'xtcoptions_item' => '',      //Default Options for XTC Profile - Item
        'count' => 10,           //Default default number of results
        'is_ascii' => FALSE,
        'case_sensitive' => FALSE,
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $has_data = FALSE;
    $elements = [];
    $elements['xtcprofile'] = [
      '#type' => 'select',
      '#title' => t('XTC Profile - List'),
      '#default_value' => $this->getSetting('xtcprofile'),
      '#description' => t('Xtended Content options for profile'),
//      '#options' => ToolBox::getPlugins($this->name),
      '#options' => ToolBox::getPlugins($this->getService(), $this->isFieldable()),
      '#required' => TRUE,
    ];
    $elements['xtcoptions'] = [
      '#type' => 'textarea',
      '#title' => t('XTC Profile options - List'),
      '#default_value' => $this->getSetting('xtcoptions'),
      '#description' => t('Xtended Content data profile. Define line by line in a "key|value" way.'),
      '#required' => FALSE,
    ];
    $elements['xtcprofile_item'] = [
      '#type' => 'select',
      '#title' => t('XTC Profile - Item'),
      '#default_value' => $this->getSetting('xtcprofile_item'),
      '#description' => t('Xtended Content options for profile'),
//      '#options' => ToolBox::getPlugins($this->name),
      '#options' => ToolBox::getPlugins($this->getService(), $this->isFieldable()),
      '#required' => TRUE,
    ];
    $elements['xtcoptions_item'] = [
      '#type' => 'textarea',
      '#title' => t('XTC Profile options - Item'),
      '#default_value' => $this->getSetting('xtcoptions_item'),
      '#description' => t('Xtended Content data profile. Define line by line in a "key|value" way.'),
      '#required' => FALSE,
    ];
    $elements['count'] = [
      '#type' => 'number',
      '#title' => t('Max result count'),
      '#default_value' => $this->getSetting('count') ?? 10,
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#min' => 1,
    ];

    return $elements;
  }

}
